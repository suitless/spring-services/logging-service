package nl.suitless.loggingservice.Services.Logic;

import com.amihaiemil.docker.Container;
import com.amihaiemil.docker.Containers;
import com.amihaiemil.docker.Docker;
import com.amihaiemil.docker.LocalDocker;
import nl.suitless.loggingservice.Domain.entities.Log;
import nl.suitless.loggingservice.Services.Interfaces.IDockerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class DockerService implements IDockerService {
    private Docker docker;
    private Pattern modulePattern;
    private String containerRegex;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Autowired
    public DockerService(@Value("${docker.socket}") String dockerSocket, @Value("${docker.regex}") String regex, @Value("${docker.containerRegex}") String containerRegex) {
        try {
            this.containerRegex = containerRegex;
            this.modulePattern = Pattern.compile(regex);
            docker = new LocalDocker(new File(dockerSocket));
        } catch (Exception e) {
            e.printStackTrace();
            //Docker could not instantiate.
        }
    }

    @Override
    public List<Log> getLogsFromService(String name) {
        List<Log> result = new ArrayList<>();
        if (isAvailable()) {
            Containers containers = docker.containers();
            Iterator<Container> itr = containers.all();

            while (itr.hasNext()) {
                try {
                    Container c = itr.next();
                    String containerName = c.inspect().getString("Name").substring(1);
                    if (containerName.equals(name)) {

                        String[] lines = c.logs().fetch().split("\n");
                        for (String line : lines) {
                            line = line.substring(8); //Remove the first 8 chars, these are garbage bade up by the docker-java api.
                            Matcher m = modulePattern.matcher(line);
                            if (m.find()) {
                                result.add(createlog(containerName, m));
                            }

                        }

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    @Override
    public List<Log> getLogsFromServiceModule(String name, String module) {
        List<Log> result = new ArrayList<>();
        if (isAvailable()) {
            Containers containers = docker.containers();
            Iterator<Container> itr = containers.all();

            while (itr.hasNext()) {
                try {
                    Container c = itr.next();
                    String containerName = c.inspect().getString("Name").substring(1);
                    if (containerName.equals(name)) {

                        String[] lines = c.logs().fetch().split("\n");
                        for (String line : lines) {
                            line = line.substring(8); //Remove the first 8 chars, these are garbage bade up by the docker-java api.
                            Matcher m = modulePattern.matcher(line);
                            if (m.find() && m.group().equals(module)) {
                                result.add(createlog(containerName, m));
                            }

                        }

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    @Override
    public List<String> getDockerServices() {
        List<String> result = new ArrayList<>();
        if (isAvailable()) {
            Containers containers = docker.containers();
            Iterator<Container> it = containers.all();
            while (it.hasNext()) {
                Container c = it.next();
                try {
                    if (c.inspect().getString("Name").matches(containerRegex)) {
                        result.add(c.inspect().getString("Name").substring(1));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    @Override
    public boolean isAvailable() {
        try {
            return docker.ping();
        } catch (IOException e) {
            return false;
        }
    }

    private Log createlog(String containerName, Matcher m) {
        try {
            return new Log(containerName, dateFormat.parse(m.group("date")), m.group("type"), m.group("executor"), m.group("class"), m.group("message"));
        } catch (ParseException e) {
            e.printStackTrace();
            //TODO: Replace runtime exception with dedicated exception
            throw new RuntimeException("Invalid date in logs");
        }
    }
}
