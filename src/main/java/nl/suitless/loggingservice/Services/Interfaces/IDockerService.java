package nl.suitless.loggingservice.Services.Interfaces;

import nl.suitless.loggingservice.Domain.entities.Log;

import java.util.List;

/**
 * @author Julius
 * @version 1.0
 * @since 19-07-2019
 */
public interface IDockerService {
    /**
     * Retrieves all logs created after date from service.
     * @param name container name
     * @return List of logs
     */
    List<Log> getLogsFromService(String name);

    /**
     * Retrieves all logs created after date from service.
     * @param name container name
     * @param module module name
     * @return List of logs
     */
    List<Log> getLogsFromServiceModule(String name, String module);

    /**
     * Gets all docker services from the daemon
     * @return List of container names
     */
    List<String> getDockerServices();

    /**
     * Returns if docker is available
     * @return boolean true if docker is available
     */
    boolean isAvailable();
}
