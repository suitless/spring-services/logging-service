package nl.suitless.loggingservice.Web.HateosResources;

import nl.suitless.loggingservice.Domain.entities.Log;
import org.springframework.hateoas.ResourceSupport;

import java.util.List;

public class LogResource extends ResourceSupport {
    private List<Log> logs;

    public  LogResource(List<Log> logs){
        this.logs = logs;
    }

    public  List<Log> getLogs(){
        return logs;
    }
}
