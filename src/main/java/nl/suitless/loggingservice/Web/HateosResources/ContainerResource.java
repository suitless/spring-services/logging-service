package nl.suitless.loggingservice.Web.HateosResources;

import org.springframework.hateoas.ResourceSupport;

import java.util.List;

public class ContainerResource extends ResourceSupport {
    private List<String> containers;

    public ContainerResource(List<String> containers){
        this.containers = containers;
    }

    public  List<String> getContainers(){
        return containers;
    }
}
