package nl.suitless.loggingservice.Web.Controllers;

import nl.suitless.loggingservice.Domain.entities.Log;
import nl.suitless.loggingservice.Services.Interfaces.IDockerService;
import nl.suitless.loggingservice.Web.HateosResources.LogResource;
import nl.suitless.loggingservice.Web.HateosResources.ContainerResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Controller
@RequestMapping("/")
public class DockerController {
    IDockerService dockerService;

    @Autowired
    public DockerController(IDockerService dockerService){
        this.dockerService = dockerService;
    }

    @GetMapping("/services")
    public ResponseEntity<ContainerResource> all() {
        List<String> services = dockerService.getDockerServices();
        ContainerResource resource = new ContainerResource(services);
        for(String container : services){
            resource.add(linkTo(methodOn(DockerController.class).service(container)).withRel("Get"));
        }
        return new ResponseEntity<>(resource, HttpStatus.OK);
    }


    @GetMapping("/{service}")
    public ResponseEntity<LogResource> service(@PathVariable("service") String name) {
        List<Log> logs = dockerService.getLogsFromService(name);
        LogResource resource = new LogResource(logs);
        return new ResponseEntity<>(resource, HttpStatus.OK);
    }


    @GetMapping("/{service}/{module}")
    public ResponseEntity<LogResource> serviceModule(@PathVariable("service") String name, @PathVariable("module") String module) {
        List<Log> logs = dockerService.getLogsFromServiceModule(name, module);
        LogResource resource = new LogResource(logs);
        return new ResponseEntity<>(resource, HttpStatus.OK);
    }
}
