package nl.suitless.loggingservice.Domain.entities;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class Log {
    private String serviceName;
    private Date date;
    private String severity;
    private String executor;
    private String classPath;
    private String message;

    public Log(String serviceName, Date date, String severity, String executor, String classPath, String message) {
        this.serviceName = serviceName;
        this.date = date;
        this.severity = severity;
        this.executor = executor;
        this.classPath = classPath;
        this.message = message;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getExecutor() {
        return executor;
    }

    public void setExecutor(String executor) {
        this.executor = executor;
    }

    public String getClassPath() {
        return classPath;
    }

    public void setClassPath(String classPath) {
        this.classPath = classPath;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
