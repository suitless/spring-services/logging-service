FROM openjdk
COPY /build/libs/com.ehvlinc.logging-service-0.0.1-SNAPSHOT.jar run.jar
EXPOSE 8888
CMD "java" "-jar" "run.jar"
